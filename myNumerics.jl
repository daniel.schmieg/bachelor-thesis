module MyNumerics

using Distributions

#  exportierte Funktionen
export updateTrajectory, fillTrajectory, from_end, from_start



"""
    function updateTrajectory(tra::Vector{Float64}, pos_index, f, para_f, D, t, Δt)
    generiert einen neuen Eintrag an der Position nach pos_index und überschreibt dabei den ältesten Eintrag
"""
function updateTrajectory(tra::Vector{Float64}, pos_index, f, para_f, D, t, Δt)
    # Berechne index für neuen eintrag
    pos_index_next = mod(pos_index,length(tra)) + 1
    
    # neuen Eintrag generieren
    tra[pos_index_next] = tra[pos_index] + f(tra[pos_index], para_f, t) * Δt + sqrt(2 * D * Δt) * rand(Normal())  
end




"""
    function fillTrajectory(N, x0:Float64, t0, f, para_f, D, Δt)
    returns an array tra with length N filled with the trajectory and the final time t
"""
function fillTrajectory(N, x0::Float64, t0, f, para_f, D, Δt)
    tra = zeros(Float64, N)
    tra[1] = x0
    t = t0
    for i in 2:N
        tra[i] = tra[i-1] + f(tra[i-1], para_f, t) * Δt + sqrt(2 * D * Δt) * rand(Normal())
        t += Δt
    end
    
    return tra, t
end

"""
    function from_end(n, current_index, N)
    gibt den Index des Eintrags n Einträge hinter current_index bei einer Arraylänge N
"""

function from_end(n, current_index, N)
    index = current_index - n

    if (index > 0)
        return index
    elseif (index < 0)
        return mod(index, N)
    else 
        return N
    end
end

"""
    function from_start(n, current_index, N)
    gibt den Index des Eintrags n Einträge nach dem ältesten Index im Array
"""
function from_start(n, current_index, N)
    index = current_index + (n+1)

    if (index < N)
        return index
    elseif (index > N)
        index = mod(index,N)
        if(index == 0)
            return N
        else
            return mod(index, N)
        end
    else
        return N
    end
end


end