# Notwendige Pakete importieren
using Distributions
using CSV, Tables, Printf

# Importiere eigene Methoden
include("./../../myNumerics.jl")
using .MyNumerics


"""
    function ode_anharmonic(t, x, para)
    anharmonische ODE für das System
"""
function ode_anharmonic(x,para, t)
    return -para[1]*x^3 + para[2]*x^2 + para[3]*x + para[4]
end

# Parameter
para = [4., 0., 4., 1.0 / 6.0]
D = 1
x = 1.5 # Position für die Konditionierung 
ε = 1e-2 # minimaler Wert für d, damit alles konvergiert
x0 = 0.0
t0 = 0
Δt = 1e-3
steps = 100000000

t = [400] # Zeitspanne in für t
τ = (5:25:305) # Zeitpunkte für τ

τ_max = maximum(τ)
t_max = maximum(t)

N = t_max + τ_max + 1

# Berechne MBR und MSD-Formel

# Array um MBR zu speichern
mbr = zeros(length(τ),length(t))

# Array um Werte für MSD-Formel zu speichern

d2 = zeros(length(τ))
db = zeros(length(τ), length(t))

# Array um Anzahl an Werten im Mittelwert zu speichern
n = zeros(length(τ))


# Generiere eine Trajektorie mit N Werten
tra, t_end = fillTrajectory(N, x0, t0, ode_anharmonic, para, D, Δt)

last_index = N #speichert den Index in tra, der als letztes beschrieben wurde

# Variable für Fortschritt

p = steps / 10
counter = 0

# Berechne MBR

for i in (1:steps)

    for j in (1:length(τ))
        # Berechne 1. Sprung
        x_0 = tra[from_start(τ_max, last_index, N)]
        d = x_0 - tra[from_start(τ_max - τ[j], last_index, N)]

        d2[j] += d*d
        
        # Berechne 2. Sprung für die t's
        for k in (1:length(t))
            b = tra[from_start(τ_max + t[k], last_index, N)] - x_0
            db[j,k] += d*b 

            if(abs(d) > ε)
                mbr[j,k] += -b/d
            end
        end
        
        if(abs(d) > ε)
            n[j] += 1
        end

    end

    # Fortschritt ausgeben
    if(counter > p)
        @printf "%.0f %% fertig \n" (i/steps*100)
        counter = 0
    end
    global counter +=1

    updateTrajectory(tra, last_index, ode_anharmonic, para, D, 0.0, Δt)
    global last_index = mod(last_index, N) + 1
end

println("Fertig")

# Normalisiere MBR und Momente
mbr = mbr ./ n
db = db ./ steps
d2 = d2 ./ steps

# Berechne MSD-Formel
msd_formula = - db ./ d2


#Speichere MBR
CSV.write("MBR_asymmetric_double_well.csv", Tables.table(hcat(τ, mbr)), header=append!(["τ/t"], string.(collect(t))))

# Speichere MSD-formel
CSV.write("MSD-Formel_asymmetric_double_well.csv", Tables.table(hcat(τ, msd_formula)), header=append!(["τ/t"], string.(collect(t))))