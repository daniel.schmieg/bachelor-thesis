# Notwendige Pakete importieren
using Distributions, CSV, Tables, Printf

# Importiere eigene Methoden
include("./../../myNumerics.jl")
using .MyNumerics

# Kraft und deren Ableitungen
"""
    function h(t, x, para)
    anharmonische ODE für das System
"""
function h(x,para, t)
    return -para[1]*x^3 + para[2]*x^2 + para[3]*x + para[4]
end

"""
    function d_h(t, x, para)
    Ableitung von h
"""
function d_h(x,para, t)
    return -3*para[1]*x^2 + 2*para[2]*x + para[3]
end

"""
    function dd_h(t, x, para)
    2te Ableitung von h
"""
function dd_h(x,para, t)
    return -6*para[1]*x + 2*para[2]
end

# Parameter
para = [4., 0., 4., 1.0 / 6.0]
D = 1
x = 1.5 # Position für die Konditionierung 
ε = 1e-2 # minimaler Wert für d, damit alles konvergiert
x0 = 0.0
t0 = 0
Δt = 1e-3
steps = 1000000000

t = [500]
τ = (5:20:505)

τ_max = maximum(τ)
t_max = maximum(t)

N = t_max + τ_max + 1

# Berechne MBR 

# Array um MBR zu speichern
mbr = zeros(length(τ),length(t))

# Array um Anzahl an Werten im Mittelwert zu speichern
n = zeros(length(τ))


# Generiere eine Trajektorie mit N Werten
tra, t_end = fillTrajectory(N, x0, t0, h, para, D, Δt)

last_index = N #speichert den Index in tra, der als letztes beschrieben wurde

# Variable für Fortschritt
p = steps / 10
counter = 0

# berechne MBR
for i in (1:steps)

    for j in (1:length(τ))
        # Berechne 1. Sprung
        x_0 = tra[from_start(τ_max, last_index, N)]
        d = x_0 - tra[from_start(τ_max - τ[j], last_index, N)]
        
        if(abs(d) > ε)
            #Berechne 2. Sprung für die t's
            for k in (1:length(t))

                b = tra[from_start(τ_max + t[k], last_index, N)] - x_0
                
                mbr[j,k] += -b/d
            end
            if(abs(d) > ε)
                n[j] += 1
            end
        end

    end

    # Fortschritt ausgeben
    if(counter > p)
        @printf "%.0f %% fertig \n" (i/steps*100)
        counter = 0
    end
    global counter +=1

    updateTrajectory(tra, last_index, h, para, D, 0.0, Δt)
    global last_index = mod(last_index, N) + 1
end

# Normalisiere MBR 
mbr = mbr ./ n
#Speichere MBR
CSV.write("MBR_asymmetric_double_well.csv", Tables.table(hcat(τ, mbr)), header=append!(["τ/t"], string.(t)))

println("MBR fertig")

#Berechne Mittelwerte

# Arrays um die Mittelwerte zu speichern

h_p = 0.0
hb = zeros(length(t))
h3b = zeros(length(t))
hh_pb= zeros(length(t))
h_ppb = zeros(length(t))
h2 = 0.0


# Generiere eine Trajektorie mit N Werten
tra, t_end = fillTrajectory(N, x0, t0, h, para, D, Δt)

last_index = N #speichert den Index in tra, der als letztes beschrieben wurde

# Berechne Mittelwerte
steps=100000000

for i in (1:steps)
    # berechne x_0
    x_0 = tra[from_start(τ_max, last_index, N)]
    # Berechne Kräfte
    h_x0 = h(x_0, para, 0.0)
    d_h_x0 = d_h(x_0, para, 0.0)

    # berechne Mittelwerte
    global h_p += d_h_x0
    global h2 += h_x0^2
    for k in (1:length(t))
        # Berechne 2. Sprung
        b = tra[from_start(τ_max + t[k], last_index, N)] - x_0

        # Berechne Mittelwerte
        hb[k] += h_x0 * b
        h3b[k] += h_x0^3 * b
        hh_pb[k] += h_x0 * d_h_x0 * b
        h_ppb[k] += dd_h(x_0, para, 0.0) * b
    end
    
    updateTrajectory(tra, last_index, h, para, D, 0.0, Δt)
    global last_index = mod(last_index, N) + 1

    
end

# Normalisiere Mittelwerte

    
h_p /= steps 
h2 /= steps 

hb ./= steps 
h3b ./= steps 
hh_pb ./= steps 
h_ppb ./= steps 


# Berechne Korrekturterme
first_order = hb ./ (2D)
second_order = h3b ./ (12*D^2) .+ 3 .* hh_pb ./ (4*D) .+ 7 .* h_ppb ./ 12 .- h2 .* hb ./ (4D^2) .- h_p .* hb ./ (2D)

#Speichere Korrekturterme
CSV.write("firstcorr_asymmetric_double_well.csv", Tables.table(hcat(t, first_order)), header=["t", "first corr"])
CSV.write("secondcorr_asymmetric_double_well.csv", Tables.table(hcat(t, second_order)), header=["t", "first corr"])
